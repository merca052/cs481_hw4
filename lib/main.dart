//Name: Jesus Mercado
//Course: CS 481 (Mobile Programing)
//Semester: Fall 2020
//Assignmnet: Homework 4 (Animations)

import 'package:flutter/material.dart';
import 'package:flutter/animation.dart';

void main() => runApp(myApp());

class myApp extends StatelessWidget{

  @override
  Widget build(BuildContext context){
    return MaterialApp(
      home: Container(
          decoration: BoxDecoration(
              image: DecorationImage(
                  image: AssetImage('assets/Space.jpeg'),
                  fit: BoxFit.cover,
               ),
             ),
          child: Container(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                AnimationApp(),
              ],
            ),
          ),
      ),
        );
  }
}

class AnimationApp extends StatefulWidget {
  _AnimationAppState createState() => _AnimationAppState();
}

//SingleTickerProviderStateMixin is the feature that
//configured vsync using this(proved this for AnimationController to refernce)
class _AnimationAppState extends State<AnimationApp> with SingleTickerProviderStateMixin {
  Animation<double> animation;
  AnimationController controller;

  @override
  void initState(){
    super.initState();
    controller = AnimationController(duration: const Duration(seconds: 2), vsync: this);
    animation = Tween<double>(begin: 0, end: 500,).animate(controller)
    ..addListener(() {
     setState(() {

     });
    });
    controller.forward();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
                     height: animation.value,
                     width: animation.value,
                      child: Image.asset('assets/Spaceship.png'),
                   );
  }

  @override
  void dispose(){
    controller.dispose();
    super.dispose();
  }
}
